﻿using System;
using System.Windows;
using Parse;
using System.Net.Http;
namespace UDelivery_Uploader
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Startup += this.Application_Startup;
            ParseClient.Initialize("l7vy3W2OHqfNGWAyCSaPX2PXkUmEXY6RsureoP1h", "KEpkm4yyKckakrCZk9KOEVvXSsJIS0bS5KVPDcxZ");

        }

        private async void Application_Startup(object sender, StartupEventArgs args)
        {
            var mainWindow = new MainWindow();
            try
            {
                await ParseAnalytics.TrackAppOpenedAsync();
            }
            catch (HttpRequestException hre)
            {
//                mainWindow.StatusLabel.Content = hre.ToString();
            }
            catch (Exception ex)
            {
//                mainWindow.StatusLabel.Content = ex.ToString();
            }
        }
    }

}
