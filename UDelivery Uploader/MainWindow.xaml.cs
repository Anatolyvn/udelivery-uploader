﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using SpreadsheetGear;
using SpreadsheetGear.Data;
using Parse;
using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using Paragraph = System.Windows.Documents.Paragraph;
using Path = System.IO.Path;


namespace UDelivery_Uploader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IEnumerable<ParseObject> companys;
        private Dictionary<string, string> idAndTitleCategory;

        public class FoodGridRow
        {
            public string category { get; set; }
            public string subcategory { get; set; }
            public string title { get; set; }
            public string description { get; set; }
            public string price { get; set; }
            public string imageFile { get; set; }
        }

        public class CompanyGridRow
        {
            public string companyName { get; set; }
            public string city { get; set; }
            public string price { get; set; }
            public string phone { get; set; }
            public string address { get; set; }
            public string logoFile { get; set; }
        }

        public class ScheduleGridRow
        {
            public string WeekDay { get; set; }
            public string StartAt { get; set; }
            public string WorkTime { get; set; }
        }

        protected string Folder { get; set; }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LoadFileButton_Click(object sender, RoutedEventArgs e)
        {
            
            CompanyGrid.Items.Clear();
            ScheduleGrid.Items.Clear();
            FoodGrid.Items.Clear();
            var dlg = new OpenFileDialog();
            dlg.Filter = "Excel Files(*.xls;*.xlsx)|*.xls;*.xlsx";
            if (dlg.ShowDialog() == true)
            {
                string directoryPath = Path.GetDirectoryName(dlg.FileName);
                Folder = directoryPath + "\\";
//                StatusLabel.Content = "Загрузка данных из файла...";
                LogTextBox.AppendText("Загрузка данных из файла..." + "\r");
                String ssFile = dlg.FileName;
                IWorkbook workbook = Factory.GetWorkbook(ssFile);
                DataSet dataSet = workbook.GetDataSet(GetDataFlags.FormattedText);
                foreach (DataRow dataRow in dataSet.Tables["Общая информация"].AsEnumerable())
                {
                    var newRow = new CompanyGridRow
                        {
                            companyName = dataRow.Field<string>("Название компании"),
                            city = dataRow.Field<string>("Город"),
                            price = dataRow.Field<string>("Мин.сумма заказа(руб.)"),
                            phone = dataRow.Field<string>("Телефон"),
                            address = dataRow.Field<string>("Адрес"),
                            logoFile = dataRow.Field<string>("Имя файла логотипа")
                        };
                    CompanyGrid.Items.Add(newRow);
                }

                foreach (DataRow dataRow in dataSet.Tables["Время работы"].AsEnumerable())
                {
                    var newRow = new ScheduleGridRow
                        {
                            WeekDay = dataRow.Field<string>("День недели"),
                            StartAt = dataRow.Field<string>("Начало работы службы доставки"),
                            WorkTime = dataRow.Field<string>("Продолжительность рабочего дня (часов)")
                        };
                    ScheduleGrid.Items.Add(newRow);
                }

                foreach (DataRow dataRow in dataSet.Tables["Список блюд"].AsEnumerable())
                {
                    var newRow = new FoodGridRow
                        {
                            category = dataRow.Field<string>("Категория (Наименование кухни)"),
                            subcategory = dataRow.Field<string>("Подкатегория (типы блюд)"),
                            title = dataRow.Field<string>("Название блюда"),
                            description = dataRow.Field<string>("Описание Блюда"),
                            price = dataRow.Field<string>("Цена, руб"),
                            imageFile = dataRow.Field<string>("Имя файла фотографии")
                        };
                    FoodGrid.Items.Add(newRow);
                }
            }
//            StatusLabel.Content = "";
        }

        private async void LoadDataToParseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CompanyGrid.Items.Count != 0 && ScheduleGrid.Items.Count != 0 && FoodGrid.Items.Count != 0)
                {
                    //Сохранение данных компании
//                    StatusLabel.Content = "Сохранение компании...";
                    LogTextBox.AppendText("Сохранение компании..." + "\r");
                    var companyGridRow = CompanyGrid.Items[0] as CompanyGridRow;
                    var addCompany = new ParseObject("Company");
                    addCompany["Name"] = companyGridRow.companyName;
                    addCompany["Phone"] = companyGridRow.phone;
                    addCompany["Adress"] = companyGridRow.address;
                    addCompany["SortNum"] = 0;
                    if (companyGridRow.logoFile != "")
                    {
                        try
                        {
                            Stream stream = File.Open(Folder + companyGridRow.logoFile, FileMode.Open);
                            var file = new ParseFile(companyGridRow.logoFile, stream);
                            addCompany["Logo"] = file;
//                            StatusLabel.Content = "Сохранение логотипа компании...";
                            LogTextBox.AppendText("Сохранение логотипа компании..." + "\r");
                        }
                        catch (Exception)
                        {

//                            StatusLabel.Content = "Файл не найдет или поврежден...";
                            LogTextBox.AppendText("Файл не найдет или поврежден..." + "\r");
                        }
                       
                    }
                    await addCompany.SaveAsync();

                    //Сохранение города
                    
//                    StatusLabel.Content = "Сохранение города";
                    LogTextBox.AppendText("Сохранение города" + "\r");
                    var newCity = new ParseObject("City");
                    newCity["Name"] = companyGridRow.city;
                    newCity["Price"] = Convert.ToInt64(companyGridRow.price);
                    newCity["IDCompany"] = addCompany.ObjectId;
                    await newCity.SaveAsync();
//                    StatusLabel.Content = "";

                    //Сохранение меню Компании

                    List<ParseObject> listAddFoods = new List<ParseObject>();
                    ParseQuery<ParseObject> getCategory = from parseObject in ParseObject.GetQuery("Category") orderby parseObject.CreatedAt descending select parseObject;
                    ParseQuery<ParseObject> getSubcategory = from parseObject in ParseObject.GetQuery("SubCategory") orderby parseObject.CreatedAt descending select parseObject;
                    IEnumerable<ParseObject> getCategoryList = await getCategory.FindAsync();
                    IEnumerable<ParseObject> getSubcategoryList = await getSubcategory.FindAsync();
                    List<ParseObject> CategoryList = getCategoryList.ToList();
                    List<ParseObject> SubcategoryList = getSubcategoryList.ToList();
                    var i = 1;

                    foreach (FoodGridRow foodGridRow in FoodGrid.Items)
                    {
                        if (foodGridRow.title != "")
                        {
//                            StatusLabel.Content = i +".Добавление в список блюда: " + foodGridRow.title + "...";
                            LogTextBox.AppendText(i + ".Добавление в список блюда: " + foodGridRow.title + "..." + "\r");

                            string IDCategory = "";
                            string IDSubCategory = "";

                            foreach (ParseObject categoryInParse in CategoryList.Where(categoryInParse => categoryInParse.Get<string>("Title") == foodGridRow.category))
                            {
                                IDCategory = categoryInParse.ObjectId;
                            }

                            if (IDCategory == "")
                            {
                                var newCategory = new ParseObject("Category");
                                newCategory["Title"] = foodGridRow.category;
                                newCategory["Visible"] = true;
                                newCategory["SortNum"] = 0;
                                await newCategory.SaveAsync();
                                IDCategory = newCategory.ObjectId;
                                CategoryList.Add(newCategory);
                            }

                            foreach (ParseObject subCategoryInParse in SubcategoryList.Where(subCategoryInParse => subCategoryInParse.Get<string>("Title") == foodGridRow.subcategory))
                            {
                                IDSubCategory = subCategoryInParse.ObjectId;
                            }


                            if (IDSubCategory == "")
                            {
                                var newSubCategory = new ParseObject("SubCategory");
                                newSubCategory["Title"] = foodGridRow.subcategory;
                                newSubCategory["Visible"] = true;
                                newSubCategory["SortNum"] = 0;
                                await newSubCategory.SaveAsync();
                                IDSubCategory = newSubCategory.ObjectId;
                                SubcategoryList.Add(newSubCategory);
                            }


                            ParseObject food = new ParseObject("Food");
                            food["IDCompany"] = addCompany.ObjectId;
                            food["IDCategory"] = IDCategory;
                            food["IDSubcategory"] = IDSubCategory;
                            food["Title"] = foodGridRow.title;
                            food["Description"] = foodGridRow.description;
                            food["Price"] = Convert.ToInt64(foodGridRow.price);
                            food["Visible"] = true;
                            
                            if (foodGridRow.imageFile != "")
                            {
//                                StatusLabel.Content = "Сохранение фотографии " + foodGridRow.imageFile + "...";
                                LogTextBox.AppendText("Сохранение фотографии " + foodGridRow.imageFile + "..." + "\r");
                                try
                                {
                                    Stream stream = File.Open(Folder + foodGridRow.imageFile, FileMode.Open);
                                    try
                                    {
                                    ParseFile file = new ParseFile(foodGridRow.imageFile, stream);
                                    await file.SaveAsync();
                                    food["Image"] = file;
//                                    StatusLabel.Content = "Сохранение фотографии " + foodGridRow.imageFile + "...";
                                    }
                                    finally
                                    {
                                        stream.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
//                                    StatusLabel.Content = "Exception" + ex.ToString();
                                    LogTextBox.AppendText("Exception" + ex.ToString() + "\r");
                                    continue;
                                }

                            }
                            listAddFoods.Add(food);
                            i++;
                        }
//                        StatusLabel.Content = "";
                    }
//                    StatusLabel.Content = "Загрузка всего списка блюд в Parse...";
                    LogTextBox.AppendText("Загрузка всего списка блюд в Parse..." + "\r");
                    await ParseObject.SaveAllAsync(listAddFoods);
//                    StatusLabel.Content = "";

                    //Сохранение графика работы
//                    StatusLabel.Content = "Сохранение графика работы...";
                    LogTextBox.AppendText("Сохранение графика работы..." + "\r");
                    foreach (object item in ScheduleGrid.Items)
                    {
                        var itemAsScheduleRow = item as ScheduleGridRow;
                        var scheduleItem = new ParseObject("TimeTable");
                        scheduleItem["WeekDay"] = itemAsScheduleRow.WeekDay;
                        scheduleItem["StartAt"] = Convert.ToInt64(TimeSpan.Parse(itemAsScheduleRow.StartAt).TotalSeconds);
                        scheduleItem["WorkTime"] = Convert.ToInt64(itemAsScheduleRow.WorkTime) * 3600;
                        scheduleItem["IDCompany"] = addCompany.ObjectId;
                        await scheduleItem.SaveAsync();

                    }

//                    StatusLabel.Content = "Данные полностью загружены в Parse";
                    LogTextBox.AppendText("Данные полностью загружены в Parse" + "\r");
                }
            }
            catch (HttpRequestException hre)
            {
//                StatusLabel.Content = hre.ToString();
                LogTextBox.AppendText(hre.ToString() + "\r");
            }
            catch (Exception ex)
            {
//                StatusLabel.Content = ex.ToString();
                LogTextBox.AppendText(ex.ToString() + "\r");
            }
        }
        
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private async void Refreshcompany_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                LogTextBox.AppendText("Получение списка компаний" + "\r");
                idAndTitleCategory = new Dictionary<string, string>();
                ParseQuery<ParseObject> query = from parseObject in ParseObject.GetQuery("Company")
                                                orderby parseObject.CreatedAt
                                                select parseObject;

                IEnumerable<ParseObject> results = await query.FindAsync();
                companys = results;

                foreach (ParseObject result in results)
                {
                    idAndTitleCategory.Add(result.ObjectId, result.Get<string>("Name"));
                }

                CompanyComboBox.ItemsSource = idAndTitleCategory.Values;
                CompanyComboBox.SelectedItem = CompanyComboBox.Items[0];
            }
            catch (HttpRequestException hre)
            {
                //                StatusLabel.Content = hre.ToString();
                LogTextBox.AppendText(hre.ToString() + "\r");
            }
            catch (Exception ex)
            {
                //                StatusLabel.Content = ex.ToString();
                LogTextBox.AppendText(ex.ToString() + "\r");
            }
        }

        private void LoadBill_Click(object sender, RoutedEventArgs e)
        {
            GetBillsOfCompany();
        }


       private async void GetBillsOfCompany()
       {
           try
           {
               string objID = "";
               int allSumPrice = 0;

               LogTextBox.AppendText("Создание PDF файла" + "\r");
               Document Doc = new Document();

               PdfWriter.GetInstance(Doc,
                                     new FileStream(
                                         Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                         string.Format("\\{0}({1}-{2}).pdf", CompanyComboBox.SelectedItem,
                                                       dateFrom.SelectedDate.Value.ToShortDateString(),
                                                       dateTo.SelectedDate.Value.ToShortDateString()), FileMode.Create));
               BaseFont helveticaLight = BaseFont.CreateFont("c:/windows/fonts/HelveticaNeueCyr-Light.otf", "cp1251", BaseFont.EMBEDDED);
               BaseFont helveticaMedium = BaseFont.CreateFont("c:/windows/fonts/HelveticaNeueCyr-Medium.otf", "cp1251", BaseFont.EMBEDDED);

               Doc.Open();

               iTextSharp.text.Paragraph head = new iTextSharp.text.Paragraph();
               head.Alignment = Element.ALIGN_CENTER;
               head.Font = new Font(helveticaLight, 14);
               head.Add(string.Format("{0}({1}-{2})", CompanyComboBox.SelectedItem,
                                                       dateFrom.SelectedDate.Value.ToShortDateString(),
                                                       dateTo.SelectedDate.Value.ToShortDateString()));
               head.Add("\r");
               head.Add("\r");
               head.Add("\r");
               Doc.Add(head);
             
               foreach (var dictionary in idAndTitleCategory.Where(dictionary => dictionary.Value == CompanyComboBox.SelectedItem))
               {
                   objID = dictionary.Key;
               }

               LogTextBox.AppendText("Загрузка блюд компании" + "\r");
               ParseQuery<ParseObject> queryFood = from parseObject in ParseObject.GetQuery("Food")
                                                    where parseObject.Get<string>("IDCompany") == objID
                                                    select parseObject;
    
               IEnumerable<ParseObject> foods = await queryFood.Limit(999).FindAsync();
               

               LogTextBox.AppendText("Загрузка заказов" + "\r");
               ParseQuery<ParseObject> queryOrder = from parseObject in ParseObject.GetQuery("Order")
                                              where parseObject.Get<string>("IDCompany") == objID 
                                              where parseObject.Get<string>("Status") == "Выполнен"
                                              where parseObject.CreatedAt >= dateFrom.SelectedDate
                                              where parseObject.CreatedAt <= dateTo.SelectedDate
                                              select parseObject;

               IEnumerable<ParseObject> orders = await queryOrder.FindAsync();
               int i = 1;
               foreach (var order in orders)
               {
                   LogTextBox.AppendText(i+".Расчет заказа " + order.ObjectId + "\r");

                   PdfPTable table = new PdfPTable(4);


                   table.AddCell(new PdfPCell(new Phrase("Дата заказа", new Font(helveticaMedium, 5))));
                   PdfPCell cellDate = new PdfPCell(new Phrase(order.CreatedAt.ToString(), new Font(helveticaMedium, 5)));
                   cellDate.Colspan = 3;
                   cellDate.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellDate);

                   table.AddCell(new PdfPCell(new Phrase("№ заказа", new Font(helveticaLight, 5))));
                   PdfPCell cellID = new PdfPCell(new Phrase(order.Get<string>("SupportID"), new Font(helveticaLight, 5)));
                   cellID.Colspan = 3;
                   cellID.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellID);

                   table.AddCell(new PdfPCell(new Phrase("Имя", new Font(helveticaLight, 5))));
                   PdfPCell cellName = new PdfPCell(new Phrase(order.Get<string>("Name"), new Font(helveticaLight, 5)));
                   cellName.Colspan = 3;
                   cellName.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellName);

                   table.AddCell(new PdfPCell(new Phrase("Телефон", new Font(helveticaLight, 5))));
                   PdfPCell cellPhone = new PdfPCell(new Phrase(order.Get<string>("Phone"), new Font(helveticaLight, 5)));
                   cellPhone.Colspan = 3;
                   cellPhone.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellPhone);

                   table.AddCell(new PdfPCell(new Phrase("Время доставки", new Font(helveticaLight, 5))));
                   PdfPCell cellTime = new PdfPCell(new Phrase(order.Get<DateTime>("Time").ToString(), new Font(helveticaLight, 5)));
                   cellTime.Colspan = 3;
                   cellTime.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellTime);

                   table.AddCell(new PdfPCell(new Phrase("Город", new Font(helveticaLight, 5))));
                   PdfPCell cellCity = new PdfPCell(new Phrase(order.Get<string>("City"), new Font(helveticaLight, 5)));
                   cellCity.Colspan = 3;
                   cellCity.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellCity);

                   table.AddCell(new PdfPCell(new Phrase("Адрес доставки", new Font(helveticaLight, 5))));
                   PdfPCell cellAdress = new PdfPCell(new Phrase(order.Get<string>("Adress") + " кв. " + order.Get<string>("Room"), new Font(helveticaLight, 5)));
                   cellAdress.Colspan = 3;
                   cellAdress.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellAdress);

                   PdfPCell cellSeparator = new PdfPCell(new Phrase(" ", new Font(helveticaLight, 7)));
                   cellSeparator.Colspan = 4;
                   cellSeparator.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cellSeparator);




                   ParseQuery<ParseObject> queryOrderDetail = from parseObject in ParseObject.GetQuery("OrderDetail")
                                                   where parseObject.Get<string>("IDOrder") == order.ObjectId
                                                   select parseObject;

                   IEnumerable<ParseObject> orderDetails = await queryOrderDetail.FindAsync();
                  
                   var sumOrder = 0;


                   table.AddCell(new PdfPCell(new Phrase("Название", new Font(helveticaMedium, 7))));
                   table.AddCell(new PdfPCell(new Phrase("ID", new Font(helveticaMedium, 7))));
                   table.AddCell(new PdfPCell(new Phrase("Количество", new Font(helveticaMedium, 7))));
                   table.AddCell(new PdfPCell(new Phrase("Цена", new Font(helveticaMedium, 7))));

                   foreach (var orderDetail in orderDetails)
                   {
                       foreach (var food in foods.Where(food => food.ObjectId == orderDetail.Get<string>("FoodID")))
                       {
                           var priceToCount  = orderDetail.Get<int>("Count")*food.Get<int>("Price");

                           table.AddCell(new PdfPCell(new Phrase(food.Get<string>("Title"), new Font(helveticaLight, 7))));
                           table.AddCell(new PdfPCell(new Phrase(food.ObjectId, new Font(helveticaLight, 7))));
                           table.AddCell(new PdfPCell(new Phrase(orderDetail.Get<int>("Count").ToString(), new Font(helveticaLight, 7))));
                           table.AddCell(new PdfPCell(new Phrase(priceToCount.ToString(), new Font(helveticaLight, 7))));
                           sumOrder += priceToCount;
                           allSumPrice += priceToCount;
                       }
                   }

                   PdfPCell cell = new PdfPCell(new Phrase("Итого " + sumOrder + " руб.", new Font(helveticaMedium, 7)));
                   cell.Colspan = 4;
                   cell.HorizontalAlignment = 2; //0=Left, 1=Centre, 2=Right
                   table.AddCell(cell);
                   Doc.Add(table);

                   iTextSharp.text.Paragraph separator2 = new iTextSharp.text.Paragraph();
                   separator2.Add("\r");
                   Doc.Add(separator2);
                   i++;
               }

               iTextSharp.text.Paragraph pAllSumPrice = new iTextSharp.text.Paragraph();
               pAllSumPrice.Alignment = Element.ALIGN_RIGHT;
               pAllSumPrice.Font = new Font(helveticaMedium, 10);
               pAllSumPrice.Add("Итого за весь период " + allSumPrice + " руб.");
               pAllSumPrice.Add("\r");

              if (percentTextBox.Text!= "" || percentTextBox.Text!="%") pAllSumPrice.Add("-" +percentTextBox.Text+ "%   " + allSumPrice*Convert.ToInt32(percentTextBox.Text)/100 + " руб.");

               LogTextBox.AppendText("Сохранение PDF документа" + "\r");
               Doc.Add(pAllSumPrice);

               Doc.Close();
               LogTextBox.AppendText("Сохранение завершено" + "\r");
           }
           catch (Exception)
           {
               LogTextBox.AppendText("Ошибка загрузки заказов, проверьте параметры запроса"+ "\r");
           }
       }

        private void LogTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
           LogTextBox.ScrollToEnd();
        }

        private void percentTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            percentTextBox.Text = "";
        }

        



       

       
    }

       }

